---
title: Sobre mim
subtitle: Tudo que você queria saber sobre mim, mas não sabia a quem perguntar
comments: false
---

Olá,

Criei este blog com a intenção de compartilhar conhecimento. Como um entusiasta do software livre e do conhecimento livre, nada mais apropriado do que liberar meus rascunhos e textos de estudo além do meu computador.

Aqui você encontrará um pouco de tudo que me cerca e me interessa, como tecnologia, software livre, conhecimento livre, Linux, Debian e skate.

Fique à vontade para me contatar nas redes sociais na parte inferior do site.


> _ “Ensinar não é transferir conhecimento, mas criar as possibilidades para a sua própria produção ou a sua construção.” _
> <br /> Paulo Freire

Abraços,
<br /> Bruno Naibert.
