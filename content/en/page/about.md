---
title: About me
subtitle: Everything you wanted to know about me but didn't know who to ask
comments: false
---

Hello,

I created this blog with the intention of sharing knowledge. As a free software and free knowledge enthusiast, nothing is more appropriate than releasing my drafts and study texts beyond my computer.

Here you will find a little bit of everything that surrounds me and interests me, such as technology, free software, free knowledge, Linux, Debian and skateboarding.

Feel free to contact me on the social networks at the bottom of the site.


> _"Teaching is not transferring knowledge, but creating the possibilities for its own production or for its construction."_  
> <br />Paulo Freire

Hugs,  
<br />Bruno Naibert.
